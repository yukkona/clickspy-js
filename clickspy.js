window.onmouseup = function(object) {
    var page = window.location.pathname;
    var token = ""; // токен, полученный при присоединении домена к системе сборы статистики
    var API_URL="http://10.0.1.215:7780/api/v1/domain/{id}/click/new?x={x}&y={y}&hour={hour}&page={page}";
    var xhr = new XMLHttpRequest();
    url = API_URL.formatUnicorn({x: object.pageX, y: object.pageY, hour: new Date().getHours(), page: page, id: token});
    console.log("URL: {url}".formatUnicorn({url: url}));
    xhr.open("GET", url, true);
    xhr.send();
};

String.prototype.formatUnicorn = String.prototype.formatUnicorn ||
function () {
    "use strict";
    var str = this.toString();
    if (arguments.length) {
        var t = typeof arguments[0];
        var key;
        var args = ("string" === t || "number" === t) ?
            Array.prototype.slice.call(arguments)
            : arguments[0];

        for (key in args) {
            str = str.replace(new RegExp("\\{" + key + "\\}", "gi"), args[key]);
        }
    }

    return str;
};